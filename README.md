# League Project
```command
By: Taha A. Ali
```

## Command Line Installation

```command
cd /path/to/project then;

composer install 
npm install
bower install
```



## Database Configuration in .env file
```command
cp .env.example .env
vi .env
And Configure DB
```

## Artisian Commands

```command
php artisan key:generate
php artisan migrate --seed
php artisan serve (Optional: Initialize Laravel Webserver)
```