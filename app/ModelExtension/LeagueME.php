<?php
/**
 * Model Extension for League
 */

namespace App\ModelExtension;

use App\Models\League;
use App\Models\Team;
use DB;

class LeagueME
{
    protected $model;
    protected $team;
    public $result = array();

    /**
     *
     * @param League $model
     * @param Team $team
     * Type-Hinting
     */
    public function __construct(League $model, Team $team)
    {
        $this->model = $model;
        $this->team = $team;

    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->team->leftJoin('league', 'teams.id', '=', 'league.team_id')
            ->orderBy('league.points', 'DESC')
            ->orderBy('league.goal_drawn', 'DESC')
            ->get();
    }
    /**
     * @return mixed
     */
    public function getTeamsIdOnly()
    {
        return $this->team->pluck('id');
    }

    /**
     * Creat League
     */
    public function createLeague()
    {
        $result = $this->model->get();
        if ($result->isEmpty()) {
            foreach ($this->getTeamsIdOnly() as $teamID) {
                $data = ['team_id' => $teamID, 'points' => 0, 'played' => 0, 'won' => 0, 'lose' => 0, 'draw' => 0, 'goal_drawn' => 0];
                $this->model->create($data);
            }
        }


    }

    /**
     * @param array $data
     * @param $team_id
     * @return mixed
     */
    public function updateLeague($data = array(), $team_id)
    {
        return $this->model->where('team_id', '=', $team_id)->update($data);
    }

    /**
     * @param $team_id
     * @return mixed
     */
    public function getLeagueByTeamId($team_id)
    {
        return $this->model->where('team_id', $team_id)->first();
    }

    /**
     * @return mixed
     */
    public function truncateLeague()
    {
        DB::table('league')->truncate();
    }


}