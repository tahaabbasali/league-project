<?php

namespace App\Http\Controllers;

use App\ModelExtension\LeagueME;
use App\ModelExtension\PlayME;

/**
 * Class MainController
 * @package App\Http\Controllers
 */
class MainController extends Controller
{
    public $league;
    public $teams;
    public $weeks;
    public $leagueMe;
    public $playMe;
    public $fixture;
    public $result = array();
    public $predictions = array();

    /**
     * MainController constructor.
     * @param LeagueME $leagueME
     * @param PlayME $playME
     */
    public function __construct(LeagueME $leagueME, PlayME $playME)
    {
        $this->leagueMe = $leagueME;
        $this->playMe = $playME;

        $this->leagueMe->createLeague();
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLeague()
    {
        $this->weeks = $this->playMe->getWeeks();
        $this->league = $this->leagueMe->getAll();
        $this->playMe->createFixture();
        $this->fixture = $this->playMe->getFixture();
        $collection = collect($this->fixture);
        $grouped = $collection->groupBy('week_id');

        $strength = $this->playMe->getStrength();

        return view(
            'pages/results',
            ['league' => $this->league,
                'matches' => $grouped->toArray(),
                'fixture' => $grouped->toArray(),
                'weeks' => $this->weeks,
                'strength' => $strength,
                'types' => array('weak', 'average', 'strong')
            ]);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshFixture()
    {
        $this->weeks = $this->playMe->getWeeks();
        $this->fixture = $this->playMe->getFixture();
        $collection = collect($this->fixture);
        $grouped = $collection->groupBy('week_id');
        return response()->json(array('weeks' => $this->weeks, 'items' => $grouped->toArray()));
    }

    /**
     * Auto Play
     */
    public function autoPlay()
    {

        $matches = $this->playMe->getAllByPlayed();
        $this->AutoSimulate($matches);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshLeague()
    {
        $this->league = $this->leagueMe->getAll();
        return response()->json($this->league);
    }

    /**
     * Reset Leagues
     */
    public function reset()
    {

        $this->playMe->truncateMatches();
        $this->leagueMe->truncateLeague();
        $this->playMe->createFixture();
    }

    /**
     * @param $matches
     */
    private function AutoSimulate($matches)
    {
        foreach ($matches as $match) {
            $homeScore = $this->playMe->ConvertStrengthToInt($match->home, 1);
            $awayScore = $this->playMe->ConvertStrengthToInt($match->away, 0);
            $home = $this->leagueMe->getLeagueByTeamId($match->home);
            $away = $this->leagueMe->getLeagueByTeamId($match->away);
            $this->playMe->storeScore($homeScore, $awayScore, $home, $away);
            $match->home_goal = $homeScore;
            $match->away_goal = $awayScore;
            $match->played = 1;
            $match->save();
        }

    }

    /**
     * @param $week
     * @return \Illuminate\Http\JsonResponse
     */
    public function nextMatch($week)
    {
        $matches = $this->playMe->getFixtureByWeekId($week);
        return response()->json(array('matches' => $matches));

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function predictions()
    {
        $finished = $this->leagueMe->getAll();
        $this->PredictionsArray($finished);
        $matches = $this->playMe->getAllByPlayed();
        $this->mergePredictions($matches);
        $collection = collect($this->predictions);
        $multiplied = $collection->map(function ($item) {
            return round((($item['points'] / $this->sumPoints()) * 100), 2);
        });

        $combine = $multiplied->all();

        //reset keys after combine
        $values = $collection->values();

        $chart = array();
        foreach ($values->all() as $key => $val) {
            array_push($chart, [$val['name'], $combine[$val['team_id']]]);
        }

        return response()->json(array('items' => $chart));
    }

    /**
     * @param $matches
     */
    private function mergePredictions($matches)
    {
        foreach ($matches as $match) {
            $homeScore = $this->playMe->ConvertStrengthToInt($match->home, 1);
            $awayScore = $this->playMe->ConvertStrengthToInt($match->away, 0);

            $points = $this->calculatePredictions($homeScore, $awayScore);
            if (isset($points['away'])) {
                foreach ($points['away'] as $key => $value) {
                    $this->predictions[$match->away][$key] += $points['away'][$key];
                }
            }
            if (isset($points['home'])) {
                foreach ($points['home'] as $key => $value) {
                    $this->predictions[$match->home][$key] += $points['home'][$key];
                }
            }
        }
    }

    /**
     * @param $data
     */
    private function PredictionsArray($data)
    {
        $collection = collect($data);
        $collection->each(function ($item) {
            $this->predictions[$item->team_id]['points'] = $item->points;
            $this->predictions[$item->team_id]['name'] = $item->name;
            $this->predictions[$item->team_id]['team_id'] = $item->team_id;
        });
    }
    /**
     * @param $homeScore
     * @param $awayScore
     * @return array
     */
    public function calculatePredictions($homeScore, $awayScore)
    {
        $points = array();
        if ($homeScore > $awayScore) {
            $points['home']['points'] = 3;
        } elseif ($awayScore > $homeScore) {
            $points['away']['points'] = 3;
        } else {
            $points['home']['points'] = 1;
            $points['away']['points'] = 1;
        }
        return $points;
    }

    /**
     * @return float|int
     */
    private function sumPoints()
    {
        return array_sum(array_map(function ($item) {
            return $item['points'];
        }, $this->predictions));
    }

    /**
     * @param $id
     * @param $column
     * @param $value
     */
    public function updateMatch($id,$column,$value)
    {
        $this->playMe->updateMatch($id,$column,$value);
        $this->leagueMe->truncateLeague();
        $this->leagueMe->createLeague();
        $matches = $this->playMe->getAllByPlayed(1);

        foreach ($matches as $match){
            $home = $this->leagueMe->getLeagueByTeamId($match->home);
            $away = $this->leagueMe->getLeagueByTeamId($match->away);

            $this->playMe->storeScore($match->home_goal, $match->away_goal, $home, $away);
        }
    }

}