@extends('layouts.default')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-6">

                <div id="loader1" class="loader"></div>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <td colspan="7" class="text-center no-border-top"><h3 class="pull-left">League Table</h3> <button  type="button" class="btn btn-primary btn-sm pull-right" id="play-all">Play All</button></td>
                    </tr>
                    <tr>
                        <td class="fb fw" bgcolor="#a9a9a9">Teams</td>
                        <td class="fb fw" bgcolor="#a9a9a9">PTS</td>
                        <td class="fb fw" bgcolor="#a9a9a9">P</td>
                        <td class="fb fw" bgcolor="#a9a9a9">W</td>
                        <td class="fb fw" bgcolor="#a9a9a9">D</td>
                        <td class="fb fw" bgcolor="#a9a9a9">L</td>
                        <td class="fb fw" bgcolor="#a9a9a9">GD</td>
                    </tr>
                    </thead>
                    <tbody id="league-table-body">
                    @if (!empty($league))
                        @foreach ($league as $lg)

                            <tr>
                                <td><img width="32" height="32" src="{{ asset('images/'.$lg->logo) }}"/> {{$lg->name}}
                                </td>
                                <td>@if(isset($lg->points)) {{$lg->points}} @else 0 @endif</td>
                                <td>@if(isset($lg->played)) {{$lg->played}} @else 0 @endif</td>
                                <td>@if(isset($lg->won)) {{$lg->won}} @else 0 @endif</td>
                                <td>@if(isset($lg->draw)) {{$lg->draw}} @else 0 @endif</td>
                                <td>@if(isset($lg->lose)) {{$lg->lose}} @else 0 @endif</td>
                                <td>@if(isset($lg->goal_drawn)) {{$lg->goal_drawn}} @else 0 @endif</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">

                <div  id="loader2" class="loader"></div>

                <table class="table table-hover" id="weekly" data-week-id="2" real=1>
                    <thead>
                    <tr>
                        <td colspan="4" class="text-center no-border-top">
                            <div class="row">
                                <div class="col-md-3"><button type="button" class="btn btn-success btn-sm text-left" disabled id="prev-week">Prev Week</button></div>
                                <div class="col-md-6 text-center"><h3>Match Results</h3></div>
                                <div class="col-md-3"><button type="button" class="btn btn-success btn-sm text-right" id="next-week">Next Week</button></div>
                            </div>
                        </td>
                    </tr>
                    </thead>
                    <tbody id="weekly-matches">
                    <tr>
                        <td colspan="3" class="text-center fb fw" bgcolor="#a9a9a9">1st Week Match Result</td>
                    </tr>

                    @if (!empty($matches))
                        @foreach ($matches[1] as $results)
                            <tr>
                                <td><img width="20" height="20" src="{{ asset('images/'.$results['home_logo']) }}"/> {{$results['home_team']}}</td>
                                <td>
                                    <input type="number" min="0" max="20" step="1" data-id="home_goal" class="homeGoal" data-match-id="{{$results['id']}}" value="{{$results['home_goal']}}">
                                    -
                                    <input type="number" min="0" max="20" step="1" data-id="away_goal" class="awayGoal" data-match-id="{{$results['id']}}" value="{{$results['away_goal']}}">
                                </td>
                                <td><img width="20" height="20" src="{{ asset('images/'.$results['away_logo']) }}"/> {{$results['away_team']}}</td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <hr style="background-color: #444444; height: 3px;">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <div id="loader4" class="loader"></div>

                <table id="predictory-data" class="table table-hover">
                    <thead>
                    <tr>
                        <td colspan="4" class="text-center no-border-top"><h3>Championship Prediction</h3></td>
                    </tr>
                    <tr>
                        <td bgcolor="#a9a9a9" class="fb fw">Teams</td>
                        <td bgcolor="#a9a9a9" class="fb fw">Percentage</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="col-md-6">

                <div id="loader3" class="loader"></div>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <td colspan="3" class=" no-border-top"><h3 class="pull-left">League Fixtures</h3><button  type="button" class="btn btn-danger btn-sm pull-right" id="reset">Reset All</button></td>
                    </tr>
                    </thead>
                    <tbody id="table-body">
                    @if (!empty($weeks))
                        @foreach($weeks as $week)
                            <tr>
                                <td colspan="3" class="fb fw" bgcolor="#a9a9a9">{{$week->name}} Matches</td>
                            </tr>
                            @if (!empty($fixture))
                                @foreach ($fixture[$week->id] as $results)

                                    <tr>
                                        <td><img width="20" height="20" src="{{ asset('images/'.$results['home_logo']) }}"/> {{$results['home_team']}}</td>
                                        <td>{{$results['home_goal']}} - {{$results['away_goal']}}</td>
                                        <td><img width="20" height="20" src="{{ asset('images/'.$results['away_logo']) }}"/> {{$results['away_team']}}</td>
                                    </tr>

                                @endforeach
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

        </div>

    </div>
@stop