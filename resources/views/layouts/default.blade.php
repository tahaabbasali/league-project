<!doctype html>
<html>
    <head>
        @include('includes.head')
    </head>
    <body class="">

        <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
            <a class="navbar-brand" href="/">League Project</a>
        </nav>

        <div id="main" class="row" style="margin:0px;">
            @yield('content')
        </div>
        @include('includes.footer')
    </body>
</html>