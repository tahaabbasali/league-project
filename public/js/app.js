function showAllLoader()
{

    $("#loader1").next().css({'opacity':'0.3'});
    $("#loader2").next().css({'opacity':'0.3'});
    $("#loader3").next().css({'opacity':'0.3'});
    $("#loader4").next().css({'opacity':'0.3'});

    $("#loader1").show();
    $("#loader2").show();
    $("#loader3").show();
    $("#loader4").show();
}

function hideAllLoader()
{
    $("#loader1").hide();
    $("#loader2").hide();
    $("#loader3").hide();
    //$("#loader4").hide();

    $("#loader1").next().css({'opacity':'1'});
    $("#loader2").next().css({'opacity':'1'});
    $("#loader3").next().css({'opacity':'1'});

}

function hideLoader(a) {
    $("#loader"+a).hide();
    $("#loader"+a).next().css({'opacity':'1'});
}
function showLoader(a) {
    $("#loader"+a).show();
    $("#loader"+a).next().css({'opacity':'0.3'});
}

$(document).ready(function () {

    showAllLoader();

    $(document).on("change paste blur", 'input.homeGoal,input.awayGoal', function(event) {

        showAllLoader();

        $.get("/api/update-match/"+ $(this).data('match-id') + "/" + $(this).data('id') + "/" + $(this).val(), {})
            .done(function (res) {
            refreshFixture();
            refreshLeague();
            refreshPredictions();
            hideLoader(2);
        });

    });

    $("span").keypress(function(e) {
        if ((event.which < 48 || event.which > 57))
            e.preventDefault();
    });

    refreshPredictions();

    $("#play-all").click(function () {
        $.get("/play-all", function () {
            showAllLoader();
            refreshFixture();
            refreshLeague();
            $('#weekly').data('week-id', 1);
            $('#weekly').attr('real', 1);
            getNextMatches();
            refreshPredictions();
            $('#prev-week').prop('disabled', true);

        });
    });

    $("#reset").click(function () {
        showAllLoader();
        $.get("api/do-reset", function () {

            window.location.href = "/";
            /*
            refreshFixture();
            refreshLeague();
            $('#weekly').data('week-id', 1);
            getNextMatches();
            $('#weekly').attr('real', 1);
            refreshPredictions();
            $('#prev-week').prop('disabled', true);*/

        });
    });

    $("#next-week").click(function () {
        showLoader(2);
        getNextMatches();

        $('#prev-week').prop('disabled', false);
    });

    $("#prev-week").click(function () {
        showLoader(2);
        getPrevMatches();
    });

    /**
     * Refresh League Fixtures
     */
    function refreshFixture() {
        $.getJSON("/api/get-fixture", function (data) {
            var showData = $('#table-body');
            var html = "";
            showData.empty();
            showData.hide();
            $.each(data.weeks, function (i, week) {

                html += "<tr><td colspan='3' class='fb fw' bgcolor='#a9a9a9'>" + week.name + " Matches</td></tr>";
                $.each(data.items[week.id], function (i, item) {
                    html += "<tr>";
                    html += "<td><img src='/images/"+ item.home_logo + "' width='30' height='30' /> " + item.home_team + "</td>";
                    html += "<td>" + item.home_goal + " - ";
                    html += item.away_goal + "</td>";
                    html += "<td><img src='/images/"+ item.away_logo + "' width='30' height='30' /> " + item.away_team + "</td>";
                    html += "</tr>";

                });

            });
            showData.html(html);
            showData.show('slow');
            hideLoader(3);
        });
    }

    /**
     * Refresh League
     */
    function refreshLeague() {
        $.getJSON("/api/get-league", function (data) {
            var showData = $('#league-table-body');
            var html = "";
            $.each(data, function (i, item) {
                html += "<tr>";
                html += "<td><img width='32' height='32' src='/images/" + item.logo + "' /> " + item.name + "</td>";
                html += "<td>" + item.points + "</td>";
                html += "<td>" + item.played + "</td>";
                html += "<td>" + item.won + "</td>";
                html += "<td>" + item.draw + "</td>";
                html += "<td>" + item.lose + "</td>";
                html += "<td>" + item.goal_drawn + "</td>";
                html += "</tr>";
            });
            showData.html(html);
            showData.show('slow');
            hideLoader(1);

        });
    }

    /**
     * Get Next Week Match
     */
    function getNextMatches() {

        $('#prev-week').prop('disabled', false);

        var weekID = $('table#weekly').data('week-id');

        if(weekID == 6){
            $('#next-week').prop('disabled', true);
        }

        $.getJSON("/api/get-next-week/" + weekID, function (data) {
            var showData = $('#weekly-matches');
            showData.empty();
            showData.hide();
            $.each(data.matches, function (i, item) {
                var html = "";
                if (i == 0) {
                    html += "<tr>"
                        + '<td colspan="3" class="fb fw text-center" bgcolor="#a9a9a9">' + item.name + ' Match Result</td>'
                        + '</tr>';
                }
                html += '<tr>'
                    + '<td><img width="20" height="20" src="/images/' + item.home_logo + '" /> ' + item.home_team + '</td>'
                    + '<td> <input type="number" min="0" max="20" step="1" data-id="home_goal" class="homeGoal" data-match-id="'+item.id+'" value="' + item.home_goal + '"> - <input type="number" min="0" max="20" step="1" data-id="away_goal" class="awayGoal" data-match-id="'+item.id+'" value="' + item.away_goal + '"></td>'
                    + '<td><img width="20" height="20" src="/images/' + item.away_logo + '"/> ' + item.away_team + '</td>'
                    + '</tr>'
                showData.append(html);

            });
            showData.show('slow');

            hideLoader(2);

        });
        $('#weekly').data('week-id', (weekID + 1));
        $('#weekly').attr('real', (parseInt($('#weekly').attr('real')) + 1))

    }

    /**
     * Get Prev Week Match
     */
    function getPrevMatches() {

        $('#next-week').prop('disabled', false);

        var weekID = $('table#weekly').data('week-id');
        weekID = weekID-2;

        if(weekID == 1){
            $('#prev-week').prop('disabled', true);
        }

        $.getJSON("/api/get-next-week/" + weekID, function (data) {
            var showData = $('#weekly-matches');
            showData.empty();
            showData.hide();
            $.each(data.matches, function (i, item) {
                var html = "";
                if (i == 0) {
                    html += "<tr>"
                        + '<td colspan="3" class="fb fw text-center" bgcolor="#a9a9a9">' + item.name + ' Match Result</td>'
                        + '</tr>';
                }
                html += '<tr>'
                    + '<td><img width="20" height="20" src="/images/' + item.home_logo + '" /> ' + item.home_team + '</td>'
                    + '<td> <input type="number" min="0" max="20" step="1" data-id="home_goal" class="homeGoal" data-match-id="'+item.id+'" value="' + item.home_goal + '"> - <input type="number" min="0" max="20" step="1" data-id="away_goal" class="awayGoal" data-match-id="'+item.id+'" value="' + item.away_goal + '"></td>'
                    + '<td><img width="20" height="20" src="/images/' + item.away_logo + '"/> ' + item.away_team + '</td>'
                    + '</tr>'
                showData.append(html);

            });
            showData.show('slow');
            hideLoader(2);

        });
        $('#weekly').data('week-id', (weekID + 1));
        $('#weekly').attr('real', (parseInt($('#weekly').attr('real')) + 1))

    }

    /**
     * Get Predictions
     */
    function refreshPredictions(){
        $.get("/api/get-predictions", function (data) {

            var showPredictions = $('#predictory-data tbody');
            showPredictions.empty();

            for(var i=0; i<data.items.length; i++)
            {
                showPredictions.append('<tr><td>'+data.items[i][0]+'</td><td>'+data.items[i][1]+'%</td></tr>');
            }
            hideLoader(4);
        });
    }


    hideAllLoader();

});