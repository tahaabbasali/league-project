<?php
use App\Models\Week;
use Illuminate\Database\Seeder;

class WeekTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function() {
            Week::insert([
                ['name' => '1st Week'],
                ['name' => '2nd Week'],
                ['name' => '3rd Week'],
                ['name' => '4th Week'],
                ['name' => '5th Week'],
                ['name' => '6th Week']
            ]);
        });
    }
}
