<?php
use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function() {
            Team::insert([
                ['name' => 'Liverpool', 'logo' => 'liverpool.png'],
                ['name' => 'Manchester City', 'logo' => 'manchester.png'],
                ['name' => 'Tottenham Hotspur', 'logo' => 't_hotspur.png'],
                ['name' => 'Arsenal', 'logo' => 'arsenal.png'],
            ]);
        });
    }
}
