<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get-fixture', "MainController@refreshFixture");
Route::get('get-league', "MainController@refreshLeague");
Route::get('do-reset', "MainController@reset");
Route::get('get-next-week/{week}', "MainController@nextMatch");
Route::get('/get-predictions', "MainController@predictions");
Route::get('/update-match/{id}/{column}/{value}',"MainController@updateMatch");